# -*- coding: utf-8 -*-

from odoo import api, fields, models,_
import csv
import base64
import datetime
from dateutil.relativedelta import relativedelta
import re
from odoo.exceptions import UserError, ValidationError,Warning


class MultiplePOConfirm(models.TransientModel):
    _name = "multiple.po.confirm"
    _description = "Multiple Po Confirm"
       
    
    mssg=fields.Char(readonly=True,default="""Are You sure,do you want to confirm orders?""")    
        
    @api.multi
    def confirm_orders(self):
        order_obj=self.env['purchase.order']    
        print ('::::::::::::========%s'%self.env.context)
        for active_id in self.env.context['active_ids']:  
            order=order_obj.browse(active_id)
            order.button_approve()

        
        
        return True
    