# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Multiple Purchase Order Confirmation',
    'version': '2.0',
    'category': 'Purchases',
    'complexity': 'easy',
    'website': '',
    'author':'Virzoteck softwares and solutions(virzoteck@gmail.com)',    
    'description': """""",
    'depends': ['stock','product','purchase'],
    'data': [
        'wizard/multiple_po_confirm.xml',
        
    ],
     
    
    'installable': True,
    'auto_install': True,
    'license': 'LGPL-3',
    "price":34,
    "currency": "EUR"    
}
